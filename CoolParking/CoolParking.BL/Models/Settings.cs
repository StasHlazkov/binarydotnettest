﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal StartBalance => 0M;
        public static int Places => 10;
        public static double PeriodForPayment => 5;
        public static int PeriodForLog => 60;
        public static Tariffs Tariffs => new Tariffs(2M, 5M, 3.5M, 1M);
        public static decimal Finecoefficient => 2.5M;
    }
}