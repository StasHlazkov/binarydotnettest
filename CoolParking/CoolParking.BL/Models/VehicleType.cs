﻿// TODO: implement enum VehicleType.
//       Items: PassengerCar, Truck, Bus, Motorcycle.

namespace CoolParking.BL.Models
{
    public enum VehicleType
    {
        PassengerCar = 5,
        Truck = 10,
        Bus = 15,
        Motorcycle = 20
    }
}
