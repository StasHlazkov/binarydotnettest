﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;
using System.ComponentModel.DataAnnotations;

namespace CoolParking.BL.Models
{
    [Serializable]
    public struct TransactionInfo
    {
        public TransactionInfo(DateTime transactionDate, string vehicleId, decimal sum) : this()
        {
            TransactionDate = transactionDate;
            VehicleId = vehicleId;
            Sum = sum;
        }
        [Required]
        public DateTime TransactionDate { get; private set; }

        [Required]
        [RegularExpression(@"^\w{2}-[0-9]{4}-\w{2}*", ErrorMessage = "ID must have format ХХ-YYYY-XX(where Х - leter, Y-number)")]
        public string VehicleId { get; private set; }

        [Required]
        public decimal Sum { get; private set; }


        public override bool Equals(object obj) => obj is TransactionInfo other && this.Equals(other);

        public bool Equals(TransactionInfo t) => TransactionDate == t.TransactionDate &&
            VehicleId == t.VehicleId &&
            Sum == t.Sum;

        public override int GetHashCode() => (TransactionDate, VehicleId, Sum).GetHashCode();

        public static bool operator ==(TransactionInfo lhs, TransactionInfo rhs) => lhs.Equals(rhs);

        public static bool operator !=(TransactionInfo lhs, TransactionInfo rhs) => !(lhs == rhs);
    }
}