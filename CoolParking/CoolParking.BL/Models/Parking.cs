﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public List<TransactionInfo> Transactions { get; private set; }
        public Parking()
        {
            Transactions = new List<TransactionInfo>();
        }

        public bool AddTransationInfo(Vehicle vehicle, decimal transactionsumma)
        {
            TransactionInfo transaction = new TransactionInfo(DateTime.Now, vehicle.Id, transactionsumma);
            Transactions.Add(transaction);
            return true;
        }
        public bool CheckIfExistTransactionByVehicleId(string Id)
        {
            var transaction = Transactions.Where(t => t.VehicleId == Id).FirstOrDefault();
            if (transaction != null)
            {
                return true;
            }
            return false;
        }
        public void ResetTransaction()
        {
            Transactions = null;
        }
    }
}