﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private static List<Vehicle> _vehicles;
        private static Parking _parking;

        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ILogService _logService;
        private static decimal _parkingBalance;


        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;
            if(_vehicles == null)
            {
                _vehicles = new List<Vehicle>();
                _parkingBalance = Settings.StartBalance;
                _parking = new Parking();
                StartParkingWork();
            }
        }
        public void AddVehicle(Vehicle newVehicle)
        {
            
            if (Vehicle.ValidId(newVehicle.Id) && Vehicle.ValidType(newVehicle.VehicleType))
            {
                var vehicle = GetVehicleByID(newVehicle.Id);
                if (vehicle != null)
                {
                    throw new ArgumentException("Vehicle with same Id doesn't exist");
                }
                else
                {
                    _vehicles.Add(newVehicle);
                }
            }
        }

        public Vehicle GetVehicleByID(string id)
        {
            return _vehicles.Where(v => v.Id == id).FirstOrDefault();
        }

        public decimal GetBalance()
        {
            return _parkingBalance;
        }

        public int GetCapacity()
        {
            return Settings.Places;
        }

        public int GetFreePlaces()
        {
            return Settings.Places - _vehicles.Count();
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _parking.Transactions.ToArray();
        }
        public bool CheckIfExistTransactionByVehicleId(string Id)
        {
            return _parking.CheckIfExistTransactionByVehicleId(Id);
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            var readVehicle = new ReadOnlyCollection<Vehicle>(_vehicles);
            return readVehicle;
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = GetVehicleByID(vehicleId);
            if (vehicle != null)
            {
                _vehicles.Remove(vehicle);
            }
            else
            {
                throw new ArgumentException("Vehicle with same Id doesn't exist");
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var vehicle = GetVehicleByID(vehicleId);
            if (vehicle != null)
            {
                vehicle.AddToBalance(sum);
            }
            else
            {
                throw new ArgumentException("Vehicle with same Id doesn't exist");
            }
        }

        public void Dispose()
        {
            StopParkingWork();
            _parkingBalance = 0;
            _vehicles = null;
            _parking.ResetTransaction();
        }

        #region Protect Methods
        protected virtual void PayForPlace(object sender, System.Timers.ElapsedEventArgs e)
        {
            foreach (var vehicle in _vehicles)
            {
                decimal transactionsumm;
                switch (vehicle.VehicleType)
                {
                    case VehicleType.PassengerCar:
                         transactionsumm = WithdrawForVehicle(vehicle, Settings.Tariffs.PassengerCar);
                        _parkingBalance += transactionsumm;
                        _parking.AddTransationInfo(vehicle, transactionsumm);
                        break;
                    case VehicleType.Truck:
                         transactionsumm = WithdrawForVehicle(vehicle, Settings.Tariffs.Truck);
                        _parkingBalance += transactionsumm;
                        _parking.AddTransationInfo(vehicle, transactionsumm);
                        break;
                    case VehicleType.Bus:
                        transactionsumm = WithdrawForVehicle(vehicle, Settings.Tariffs.Bus);
                        _parkingBalance += transactionsumm;
                        _parking.AddTransationInfo(vehicle, transactionsumm);
                        break;
                    case VehicleType.Motorcycle:
                        transactionsumm = WithdrawForVehicle(vehicle, Settings.Tariffs.Motorcycle);
                        _parkingBalance += transactionsumm;
                        _parking.AddTransationInfo(vehicle, transactionsumm);
                        break;
                    default:
                        throw new Exception($"We can't get money from {vehicle.Id}. Uncorrect vehicleType");

                }
            }
        }

        protected virtual decimal WithdrawForVehicle(Vehicle vehicle, decimal money)
        {
            decimal charge = 0;
            if (money > 0)
            {
                if (vehicle.Balance - money > 0)
                {
                    vehicle.WithdrawFromBalance(money);
                    charge = money;
                }
                else if (vehicle.Balance >= 0)
                {
                    decimal negativeBalance = (Decimal)Math.Abs(vehicle.Balance - money);
                    charge = (negativeBalance * Settings.Finecoefficient) + vehicle.Balance;
                    vehicle.WithdrawFromBalance(charge);
                }
                else
                {
                    charge = money * Settings.Finecoefficient;
                    vehicle.WithdrawFromBalance(charge);
                }
                return charge;
            }
            else
            {
                throw new ArgumentException("Summa for withdraw is negative!");
            }
        }

        protected virtual void WriteLog(object sender, ElapsedEventArgs e)
        {
            foreach (var parkingtransaction in _parking.Transactions.ToList())
            {
                string logRaw = $"{parkingtransaction.TransactionDate.ToString()} {parkingtransaction.Sum} money withdrawn from vehicle with Id = '{parkingtransaction.VehicleId}'.";
                    _logService.Write(logRaw);
            }
        }
        #endregion
        private void StartParkingWork()
        {
            _withdrawTimer.Interval = Settings.PeriodForPayment;
            _logTimer.Interval = Settings.PeriodForLog;
            _withdrawTimer.Elapsed += PayForPlace;
            _logTimer.Elapsed += WriteLog;

            _withdrawTimer.Start();
            _logTimer.Start();
        }


        private void StopParkingWork()
        {
            _withdrawTimer.Stop();
            _logTimer.Stop();
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
        }
    }
}