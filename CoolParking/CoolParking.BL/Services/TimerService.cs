﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using CoolParking.BL.Interfaces;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer _timer;
        private double _interval;

        public event ElapsedEventHandler Elapsed;
        public TimerService()
        {
            _timer = new Timer();
        }
        public double Interval
        {
            get
            {
                if (_interval == 0)
                {
                    _interval = _interval * 1000;
                }
                return _interval;
            }
            set
            {
                _interval = value * 1000;
            }
        }

        public void Dispose()
        {
            _timer.Dispose();
        }

        public void Start()
        {
            try
            {
                _timer.Interval = Interval;
                _timer.Elapsed += Elapsed;
                _timer.AutoReset = true;
                _timer.Enabled = true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void Stop()
        {
            _timer.Enabled = false;
        }


    }
}