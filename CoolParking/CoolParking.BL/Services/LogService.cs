﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private readonly string _logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
        private readonly string _logPath;
        public LogService()
        {
            _logPath = _logFilePath;
        }
        public LogService(string logPath)
        {
            _logPath = logPath;
        }
        public string LogPath => _logPath;



        public string Read()
        {
            try
            {
                using (FileStream fstream = File.OpenRead(LogPath))
                {
                    using (StreamReader sw = new StreamReader(fstream))
                    {
                        return sw.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return ex.Message;
            }
        }

        public void Write(string logInfo)
        {
            try
            {
                using (FileStream fstream = new FileStream(LogPath, FileMode.Append))
                {
                    using (StreamWriter sw = new StreamWriter(fstream))
                    {
                        sw.WriteLine(logInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public TransactionInfo? DeserealizateLastTransaction()
        {
            TransactionInfo[] transactions;
            try
            {
                using (FileStream fstream = File.OpenRead(LogPath))
                {
                    XmlSerializer formatter = new XmlSerializer(typeof(TransactionInfo));
                    transactions = (TransactionInfo[])formatter.Deserialize(fstream);
                    return transactions[transactions.Length - 1];
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public bool SerealizateTransaction(TransactionInfo transaction)
        {
            try
            {
                using (FileStream fstream = new FileStream(LogPath, FileMode.OpenOrCreate))
                {
                    XmlSerializer formatter = new XmlSerializer(typeof(TransactionInfo));
                    formatter.Serialize(fstream, transaction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }           
        }

        public TransactionInfo[] DeserealizateAllTransaction()
        {
            TransactionInfo[] transactions;
            try
            {
                using (FileStream fstream = File.OpenRead(LogPath))
                {
                    XmlSerializer formatter = new XmlSerializer(typeof(TransactionInfo));
                    transactions = (TransactionInfo[])formatter.Deserialize(fstream);
                    return transactions;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }
}