﻿using CoolParking.BL.Models;

namespace CoolParking.BL.Interfaces
{
    public interface ILogService
    {
        string LogPath { get; }
        void Write(string logInfo);
        string Read();
        bool SerealizateTransaction(TransactionInfo transaction);
        TransactionInfo[] DeserealizateAllTransaction();
        TransactionInfo? DeserealizateLastTransaction();
    }
}